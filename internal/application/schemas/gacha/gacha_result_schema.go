package schema_gacha

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type GachaResult struct {
	CharacterId value_character.CharacterId
	ItemId      int32
	ItemAmount  uint32
}
