package schema_room_object

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
)

// User main room and sub room
type BuySetPlayerRoomObject struct {
	ManagedRoomId uint
	RoomObjectId  uint32
	BuyAmount     uint8
	// RoomNo (1: main / 2: sub)
	FloorId value_room.FloorId
	// Mystery (0: default)
	GroupId int64
	// Room object data
	ArrangeData []model_user.ManagedRoomArrangeData
}
