package schema_character

type ConsumeItem struct {
	ItemId int32
	Count  uint16
}
