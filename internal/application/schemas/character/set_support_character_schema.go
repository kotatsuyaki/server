package schema_character

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type SetSupportCharacterRequestSchemaSupportCharacter struct {
	Name                string
	ManagedSupportId    value_user.ManagedSupportId
	ManagedWeaponIds    []value_user.ManagedWeaponId
	ManagedCharacterIds []value_user.ManagedCharacterId
	Active              value.BoolLikeUInt8
}

type SetSupportCharacterRequestSchema struct {
	SupportCharacters []SetSupportCharacterRequestSchemaSupportCharacter
}
