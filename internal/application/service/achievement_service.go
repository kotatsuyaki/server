package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type AchievementService interface {
	// Get All Player Achievement (WIP)
	GetAllPlayerAchievement(internalUserId uint) ([]model_user.UserAchievement, error)
	// Set Player Achievement (WIP)
	SetPlayerAchievement(internalUserId uint, achievementId uint64) (model_user.User, error)
	// Shown Player Achievement (WIP)
	ShownPlayerAchievement(internalUserId uint, titleTypes []value_character.TitleType) error
}

type achievementService struct {
	uu usecase.UserUsecase
}

func NewAchievementService(uu usecase.UserUsecase) AchievementService {
	return &achievementService{uu}
}

func (s *achievementService) GetAllPlayerAchievement(internalUserId uint) ([]model_user.UserAchievement, error) {
	// // Get user
	// user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
	// 	Achievements: true,
	// })
	// if err != nil {
	// 	return nil, err
	// }
	return []model_user.UserAchievement{}, errors.New("not implemented")
}

func (s *achievementService) SetPlayerAchievement(internalUserId uint, achievementId uint64) (model_user.User, error) {
	// // Get user
	// user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
	// 	Achievements: true,
	// })
	// if err != nil {
	// 	return model_user.User{}, err
	// }

	// // achievement, err := user.GetAchievement(achievementId)
	// // if err != nil {
	// // 	return model_user.User{}, err
	// // }

	// user.CurrentAchievementId = achievementId
	// // Save user
	// user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	// if err != nil {
	// 	return model_user.User{}, err
	// }
	// return *user, nil
	return model_user.User{}, errors.New("not implemented")
}

func (s *achievementService) ShownPlayerAchievement(internalUserId uint, titleTypes []value_character.TitleType) error {
	// // Get user
	// user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
	// 	Achievements: true,
	// })
	// if err != nil {
	// 	return err
	// }

	// // TODO: write the shown logic

	// // Save user
	// user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	// if err != nil {
	// 	return err
	// }
	// return nil
	return errors.New("not implemented")
}
