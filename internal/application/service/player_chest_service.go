package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_chest "gitlab.com/kirafan/sparkle/server/internal/domain/model/chest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type PlayerChestService interface {
	// Draw player chest
	DrawPlayerChest(internalUserId uint, chestId uint, count uint8) (model_user.User, model_chest.Chest, []model_chest.PrizeResult, error)
	// Get all player chest
	GetAllPlayerChest(internalUserId uint) ([]model_chest.Chest, error)
	// Get step player chest
	GetStepPlayerChest(internalUserId uint, chestId int, step uint16) ([]model_chest.ResetChestPrize, error)
	// Reset player chest
	ResetPlayerChest(internalUserId uint, chestId int) (model_chest.Chest, error)
}

type playerChestService struct {
	uu usecase.UserUsecase
}

func NewPlayerChestService(uu usecase.UserUsecase) PlayerChestService {
	return &playerChestService{uu}
}

func (s *playerChestService) DrawPlayerChest(internalUserId uint, chestId uint, count uint8) (model_user.User, model_chest.Chest, []model_chest.PrizeResult, error) {
	return model_user.User{}, model_chest.Chest{}, []model_chest.PrizeResult{}, errors.New("not implemented")
}

func (s *playerChestService) GetAllPlayerChest(internalUserId uint) ([]model_chest.Chest, error) {
	return nil, errors.New("not implemented")
}

func (s *playerChestService) GetStepPlayerChest(internalUserId uint, chestId int, step uint16) ([]model_chest.ResetChestPrize, error) {
	return nil, errors.New("not implemented")
}

func (s *playerChestService) ResetPlayerChest(internalUserId uint, chestId int) (model_chest.Chest, error) {
	return model_chest.Chest{}, errors.New("not implemented")
}
