package service

import (
	"time"

	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type InformationService interface {
	GetAllInformation(platform value_version.Platform) ([]model_information.Information, error)
}

type informationService struct {
}

func NewInformationService() InformationService {
	return &informationService{}
}

func (s *informationService) GetAllInformation(platform value_version.Platform) ([]model_information.Information, error) {
	resp := []model_information.Information{
		{
			Id:          772,
			Sort:        1,
			Platform:    value_version.PlatformUnspecified,
			StartAt:     time.Date(2022, 11, 7, 16, 10, 0, 0, time.UTC),
			EndAt:       time.Date(2099, 1, 1, 23, 59, 59, 0, time.UTC),
			DispStartAt: time.Date(2022, 11, 7, 17, 0, 0, 0, time.UTC),
			DispEndAt:   time.Date(2099, 1, 1, 23, 59, 59, 0, time.UTC),
			IsFeatured:  value.BoolLikeUIntFalse,
			ImgId:       "G00901",
			Url:         "https://web.archive.org/web/20221104080546/https://kirara.star-api.com/cat_news/information/1760/",
		},
	}
	return resp, nil
}
