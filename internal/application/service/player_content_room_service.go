package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type PlayerContentRoomService interface {
	// Get All Player Content Room Member (WIP)
	GetAllPlayerContentRoomMember(internalUserId uint) ([]model_user.ManagedCharacter, error)
	// Set All Player Content Room Member (WIP)
	SetAllPlayerContentRoomMember(internalUserId uint, managedCharacterIds []int32, titleType value_character.TitleType) error
	// Set Shown Player Content Room (WIP)
	SetShownPlayerContentRoom(internalUserId uint, titleTypes []value_character.TitleType) ([]model_user.OfferTitleType, error)
}

type playerContentRoomService struct {
	uu usecase.UserUsecase
}

func NewPlayerContentRoomService(uu usecase.UserUsecase) PlayerContentRoomService {
	return &playerContentRoomService{uu}
}

func (s *playerContentRoomService) GetAllPlayerContentRoomMember(internalUserId uint) ([]model_user.ManagedCharacter, error) {
	// Get user
	// user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
	// 	ContentRoom: true,
	// })
	// if err != nil {
	// 	return nil, err
	// }
	// return user.ContentRoom.Members, nil
	return []model_user.ManagedCharacter{}, errors.New("not implemented")
}

func (s *playerContentRoomService) SetAllPlayerContentRoomMember(internalUserId uint, managedCharacterIds []int32, titleType value_character.TitleType) error {
	// Get user
	// user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
	// 	ContentRoom: true,
	// })
	// if err != nil {
	// 	return err
	// }
	// // Set members
	// user.ContentRoom.Members = make([]model_user.UserContentRoomMember, len(titleTypes))
	// for i, titleType := range titleTypes {
	// 	user.ContentRoom.Members[i] = model_user.UserContentRoomMember{
	// 		TitleType: value_character.TitleType(titleType),
	// 	}
	// }
	// // Save user
	// _, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	// return err
	return errors.New("not implemented")
}

func (s *playerContentRoomService) SetShownPlayerContentRoom(internalUserId uint, titleTypes []value_character.TitleType) ([]model_user.OfferTitleType, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{OfferTitleTypes: true})
	if err != nil {
		return []model_user.OfferTitleType{}, errors.New("user not found")
	}

	for i := range titleTypes {
		user.SetShownPlayerContentRoom(titleTypes[i])
	}

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{OfferTitleTypes: true})
	if err != nil {
		return []model_user.OfferTitleType{}, errors.New("database save error")
	}
	return user.OfferTitleTypes, err
}
