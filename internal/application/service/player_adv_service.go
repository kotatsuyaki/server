package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type PlayerAdvService interface {
	AddPlayerAdv(internalUserId uint, advId uint64, stepCode value_user.StepCode) error
}

type playerAdvService struct {
	uu usecase.UserUsecase
}

func NewPlayerAdvService(uu usecase.UserUsecase) PlayerAdvService {
	return &playerAdvService{uu}
}

func (s *playerAdvService) AddPlayerAdv(internalUserId uint, advId uint64, stepCode value_user.StepCode) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{AdvIds: true})
	if err != nil {
		return err
	}

	user.AddClearedAdv(advId)
	user.UpdateStepCode(stepCode)

	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{AdvIds: true}); err != nil {
		return err
	}
	return nil
}
