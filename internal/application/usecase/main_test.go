package usecase

import (
	"os"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gorm.io/gorm"
)

var db *gorm.DB
var logRepo repository.LoggerRepository

func TestMain(m *testing.M) {
	logger := database.InitLogger()
	logRepo = database.InitLoggerRepo(logger)
	db = database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	exitVal := m.Run()
	os.Exit(exitVal)
}
