package usecase

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestUsecase interface {
	GetPart1Quests(internalUserId uint) ([]*model_quest.Quest, error)
	GetPart2Quests(internalUserId uint) ([]*model_quest.Quest, error)
	GetQuestCategory(questId uint) (*value_quest.QuestCategoryType, error)
	GetQuest(questId uint) (*model_quest.Quest, error)
}

type questUsecase struct {
	qr     repository.QuestRepository
	logger repository.LoggerRepository
}

func NewQuestUsecase(qr repository.QuestRepository, logger repository.LoggerRepository) QuestUsecase {
	return &questUsecase{qr: qr, logger: logger}
}

func (uc *questUsecase) GetQuest(questId uint) (*model_quest.Quest, error) {
	foundQuest, err := uc.qr.FindQuest(&model_quest.Quest{Id: questId}, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuest, nil
}

func (uc *questUsecase) GetPart1Quests(internalUserId uint) ([]*model_quest.Quest, error) {
	// NOTE: Since GORM ignores zero value, we need to use map instead of struct
	criteria := map[string]interface{}{
		"category": value_quest.QuestCategoryTypeMainPart1,
	}
	foundQuests, err := uc.qr.FindQuests(nil, criteria, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetPart2Quests(internalUserId uint) ([]*model_quest.Quest, error) {
	query := &model_quest.Quest{
		Category: value_quest.QuestCategoryTypeMainPart2,
	}
	foundQuests, err := uc.qr.FindQuests(query, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetQuestCategory(questId uint) (*value_quest.QuestCategoryType, error) {
	category, err := uc.qr.GetQuestCategory(questId)
	return category, err
}
