package usecase

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type EvoTableLimitBreakUsecase interface {
	GetLimitBreakRecipe(recipeId uint) (*model_evo_table.EvoTableLimitBreak, error)
}

type evoTableLimitBreakUsecase struct {
	rp     repository.EvoTableLimitBreakRepository
	logger repository.LoggerRepository
}

func NewEvoTableLimitBreakUsecase(rp repository.EvoTableLimitBreakRepository, logger repository.LoggerRepository) EvoTableLimitBreakUsecase {
	return &evoTableLimitBreakUsecase{rp, logger}
}

func (u *evoTableLimitBreakUsecase) GetLimitBreakRecipe(recipeId uint) (*model_evo_table.EvoTableLimitBreak, error) {
	res, err := u.rp.FindByRecipeId(recipeId)
	if err != nil {
		return nil, err
	}
	return res, nil
}
