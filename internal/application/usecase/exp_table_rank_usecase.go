package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ExpTableRankUsecase interface {
	GetNextExpTableRank(currentExp uint64) (*model_exp_table.ExpTableRank, error)
}

type expTableRankUsecase struct {
	rp     repository.ExpTableRankRepository
	logger repository.LoggerRepository
}

func NewExpTableRankUsecase(rp repository.ExpTableRankRepository, logger repository.LoggerRepository) ExpTableRankUsecase {
	return &expTableRankUsecase{rp, logger}
}

func (uc *expTableRankUsecase) GetNextExpTableRank(currentExp uint64) (*model_exp_table.ExpTableRank, error) {
	criteria := map[string]interface{}{
		"total_exp > ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableRank(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}
