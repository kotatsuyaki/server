package usecase

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type EvoTableWeaponUsecase interface {
	GetEvolutionRecipe(weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error)
}

type evoTableWeaponUsecase struct {
	rp     repository.EvoTableWeaponRepository
	logger repository.LoggerRepository
}

func NewEvoTableWeaponUsecase(rp repository.EvoTableWeaponRepository, logger repository.LoggerRepository) EvoTableWeaponUsecase {
	return &evoTableWeaponUsecase{rp, logger}
}

func (u *evoTableWeaponUsecase) GetEvolutionRecipe(weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error) {
	res, err := u.rp.FindByWeaponId(weaponId)
	if err != nil {
		return nil, err
	}
	return res, nil
}
