package usecase

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type WeaponRecipeUsecase interface {
	GetWeaponRecipeById(recipeId uint32) (*model_weapon.WeaponRecipe, error)
}

type weaponRecipeUsecase struct {
	rp     repository.WeaponRecipeRepository
	logger repository.LoggerRepository
}

func NewWeaponRecipeUsecase(rp repository.WeaponRecipeRepository, logger repository.LoggerRepository) WeaponRecipeUsecase {
	return &weaponRecipeUsecase{rp, logger}
}

func (uc *weaponRecipeUsecase) GetWeaponRecipeById(recipeId uint32) (*model_weapon.WeaponRecipe, error) {
	recipe, err := uc.rp.FindByRecipeId(recipeId)
	if err != nil {
		return nil, err
	}
	return recipe, nil
}
