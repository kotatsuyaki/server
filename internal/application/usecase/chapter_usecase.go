package usecase

import (
	"time"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ChapterUsecase interface {
	GetAll() ([]*model_chapter.Chapter, *time.Time, error)
}

type chapterUsecase struct {
	cr     repository.ChapterRepository
	logger repository.LoggerRepository
}

func NewChapterUsecase(qr repository.ChapterRepository, logger repository.LoggerRepository) ChapterUsecase {
	return &chapterUsecase{cr: qr, logger: logger}
}

func (cu *chapterUsecase) GetAll() ([]*model_chapter.Chapter, *time.Time, error) {
	chapters, err := cu.cr.GetAll()
	if err != nil {
		return nil, nil, err
	}
	var latestTime time.Time
	for _, chapter := range chapters {
		if chapter.NewDispEndAt != nil && chapter.NewDispEndAt.After(latestTime) {
			latestTime = *chapter.NewDispEndAt
		}
	}
	return chapters, &latestTime, nil
}
