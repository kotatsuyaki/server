package usecase

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type EventQuestUsecase interface {
	GetEventQuest(internalUserId uint, questId uint) (*model_quest.EventQuest, error)
	GetEventQuests(internalUserId uint) ([]*model_quest.EventQuestWrapper, error)
	GetEventQuestPeriods(internalUserId uint) ([]*model_quest.EventQuestPeriod, error)
}

type eventQuestUsecase struct {
	eqr repository.EventQuestRepository
	ur  repository.UserRepository
}

func NewEventQuestUsecase(eqr repository.EventQuestRepository, ur repository.UserRepository) EventQuestUsecase {
	return &eventQuestUsecase{eqr, ur}
}

func (uc *eventQuestUsecase) GetEventQuest(internalUserId uint, eventQuestId uint) (*model_quest.EventQuest, error) {
	eventQuest, err := uc.eqr.GetEventQuest(internalUserId, eventQuestId)
	if err != nil {
		return nil, err
	}
	return eventQuest, nil
}

func (uc *eventQuestUsecase) GetEventQuests(internalUserId uint) ([]*model_quest.EventQuestWrapper, error) {
	eventQuests, err := uc.eqr.GetEventQuests(internalUserId)
	if err != nil {
		return nil, err
	}
	clearRanks, err := uc.ur.GetUserQuestLogClearRanks(internalUserId)
	if err != nil {
		return nil, err
	}
	eventQuestWrappers := make([]*model_quest.EventQuestWrapper, len(eventQuests))
	for i, eventQuest := range eventQuests {
		eventQuestWrappers[i] = &model_quest.EventQuestWrapper{
			EventQuest: *eventQuest,
			ClearRank:  clearRanks.GetClearRank(eventQuest.QuestId),
			IsRead:     value.BoolLikeUIntFalse,
			// Unused values?
			OrderDaily: 0,
			OrderTotal: 0,
			// Maybe chest event values?
			ChestCostItemId:     -1,
			ChestCostItemAmount: 0,
		}
	}
	return eventQuestWrappers, nil
}

func (uc *eventQuestUsecase) GetEventQuestPeriods(internalUserId uint) ([]*model_quest.EventQuestPeriod, error) {
	eventQuestPeriods, err := uc.eqr.GetEventQuestPeriods(internalUserId)
	if err != nil {
		return nil, err
	}
	return eventQuestPeriods, nil
}
