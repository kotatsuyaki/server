package usecase

import (
	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
)

type LevelTableTownFacilityUsecase interface {
	GetCurrentLevelTableTownFacility(levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error)
	GetNextLevelTableTownFacility(levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error)
}

type levelTableTownFacilityUsecase struct {
	rp     repository.LevelTableTownFacilityRepository
	logger repository.LoggerRepository
}

func NewLevelTableTownFacilityUsecase(rp repository.LevelTableTownFacilityRepository, logger repository.LoggerRepository) LevelTableTownFacilityUsecase {
	return &levelTableTownFacilityUsecase{rp, logger}
}

func (uc *levelTableTownFacilityUsecase) GetCurrentLevelTableTownFacility(levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error) {
	levelTableTownFacility, err := uc.rp.FindLevelTableTownFacility(&model_level_table.LevelTableTownFacility{
		LevelUpListId: levelUpListId,
		TargetLevel:   currentLevel,
	})
	if err != nil {
		return nil, err
	}
	return levelTableTownFacility, nil
}

func (uc *levelTableTownFacilityUsecase) GetNextLevelTableTownFacility(levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error) {
	levelTableTownFacility, err := uc.rp.FindLevelTableTownFacility(&model_level_table.LevelTableTownFacility{
		LevelUpListId: levelUpListId,
		TargetLevel:   currentLevel + 1,
	})
	if err != nil {
		return nil, err
	}
	return levelTableTownFacility, nil
}
