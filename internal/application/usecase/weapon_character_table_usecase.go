package usecase

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponCharacterTableUsecase interface {
	GetWeaponCharacterTableById(characterId value_character.CharacterId) (*value_weapon.WeaponId, error)
}

type weaponCharacterTableUsecase struct {
	rp     repository.WeaponCharacterTableRepository
	logger repository.LoggerRepository
}

func NewWeaponCharacterTableUsecase(rp repository.WeaponCharacterTableRepository, logger repository.LoggerRepository) WeaponCharacterTableUsecase {
	return &weaponCharacterTableUsecase{rp, logger}
}

func (uc *weaponCharacterTableUsecase) GetWeaponCharacterTableById(characterId value_character.CharacterId) (*value_weapon.WeaponId, error) {
	character, err := uc.rp.FindByCharacterId(characterId)
	if err != nil {
		return nil, err
	}
	return &character.WeaponId, nil
}
