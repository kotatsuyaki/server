## domain/usecase

This is the domain usecase of business logic at DDD.
All core logics must be keep in usecase.

main <-> middleware <-> router(interfaces) <-> **usecase** <-> repository <-> model(gorm) <-> db(persistence)
