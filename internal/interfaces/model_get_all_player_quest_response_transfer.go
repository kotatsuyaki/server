package interfaces

import (
	schema_quest "gitlab.com/kirafan/sparkle/server/internal/application/schemas/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toGetAllPlayerQuestResponse(res *schema_quest.AllQuestInfoWithClearRanksSchema, success *response.BaseResponse) *GetAllPlayerQuestResponse {
	// Format resp->quests field
	outQuestPart1s := make([]QuestsArrayObject, len(res.QuestPart1s))
	for i := range outQuestPart1s {
		var copiedQuest QuestsArrayObjectQuest
		calc.Copy(&copiedQuest, &res.QuestPart1s[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart1s[i] = QuestsArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}
	// Format resp->questPart2s field
	outQuestPart2s := make([]Questpart2sArrayObject, len(res.QuestPart2s))
	for i := range outQuestPart2s {
		var copiedQuest Questpart2sArrayObjectQuest
		calc.Copy(&copiedQuest, &res.QuestPart2s[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart2s[i] = Questpart2sArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}

	// Format resp->questPart2s field
	outEventQuests := make([]EventQuestsArrayObject, len(res.EventQuests))
	for i := range outEventQuests {
		var out EventQuestsArrayObject
		calc.Copy(&out, &res.EventQuests[i])
		var outEventQuestQuest EventQuestsArrayObjectEventquestQuest
		calc.Copy(&outEventQuestQuest, &res.EventQuests[i].EventQuest.Quest)
		calc.Copy(&outEventQuestQuest, &res.EventQuests[i].EventQuest.Quest.QuestFirstClearReward)
		outEventQuestQuest.Gem1 = outEventQuestQuest.QuestFirstClearReward.Gem
		outEventQuestQuest.Gold1 = outEventQuestQuest.QuestFirstClearReward.Gold
		outEventQuestQuest.Id = int64(res.EventQuests[i].EventQuest.Quest.Id)
		// Fill empty array
		if len(outEventQuestQuest.QuestNpcs) == 0 {
			outEventQuestQuest.QuestNpcs = make([]interface{}, 0)
		}
		// Fill duplicated value
		outEventQuestQuest.WaveIds = []int64{
			outEventQuestQuest.WaveId1,
			outEventQuestQuest.WaveId2,
			outEventQuestQuest.WaveId3,
			outEventQuestQuest.WaveId4,
			outEventQuestQuest.WaveId5,
		}
		// Fill Mystery values
		outEventQuestQuest.MainFlg = 0
		outEventQuestQuest.Section = -1
		out.EventQuest.StartAt = response.ToSparkleTime(res.EventQuests[i].EventQuest.StartAt)
		out.EventQuest.EndAt = response.ToSparkleTime(res.EventQuests[i].EventQuest.EndAt)
		// FIXME: This is a workaround to avoid wrong model
		out.EventQuest.CondEventQuestIds = make([]interface{}, 0)
		out.EventQuest.Quest = outEventQuestQuest
		// Inject clearRank
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(out.EventQuest.Quest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		out.ClearRank = clearRank
		outEventQuests[i] = out
	}

	outEventQuestPeriods := make([]EventQuestPeriodsArrayObject, len(res.EventQuestPeriods))
	for i := range outEventQuestPeriods {
		var out EventQuestPeriodsArrayObject
		calc.Copy(&out, &res.EventQuestPeriods[i])
		out.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].StartAt)
		out.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EndAt)
		var lossTimeEndAt *string
		if res.EventQuestPeriods[i].LossTimeEndAt != nil {
			lossTimeEndAt = calc.ToPtr(response.ToSparkleTime(*res.EventQuestPeriods[i].LossTimeEndAt))
		}
		out.LossTimeEndAt = lossTimeEndAt

		outEventQuestPeriodGroups := make([]EventQuestPeriodGroupsArrayObject, len(res.EventQuestPeriods[i].EventQuestPeriodGroups))
		for i2 := range outEventQuestPeriodGroups {
			var outPeriodGroup EventQuestPeriodGroupsArrayObject
			calc.Copy(&outPeriodGroup, &res.EventQuestPeriods[i].EventQuestPeriodGroups[i2])
			outPeriodGroup.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].StartAt)
			outPeriodGroup.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EndAt)
			outEventQuestPeriodGroupQuests := make([]EventQuestPeriodGroupQuestsArrayObject, len(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests))
			for i3 := range outEventQuestPeriodGroupQuests {
				var outPeriodGroupQuest EventQuestPeriodGroupQuestsArrayObject
				calc.Copy(&outPeriodGroupQuest, &res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3])
				outPeriodGroupQuest.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3].StartAt)
				outPeriodGroupQuest.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3].EndAt)
				// FIXME: This is a workaround to avoid wrong model
				outPeriodGroupQuest.CondEventQuestIds = make([]interface{}, 0)
				outEventQuestPeriodGroupQuests[i3] = outPeriodGroupQuest
			}
			outPeriodGroup.EventQuestPeriodGroupQuests = outEventQuestPeriodGroupQuests
			outEventQuestPeriodGroups[i2] = outPeriodGroup
		}
		out.EventQuestPeriodGroups = outEventQuestPeriodGroups
		outEventQuestPeriods[i] = out
	}

	return &GetAllPlayerQuestResponse{
		Quests:                    outQuestPart1s,
		QuestPart2s:               outQuestPart2s,
		EventQuests:               outEventQuests,
		EventQuestPeriods:         outEventQuestPeriods,
		QuestStaminaReductions:    []QuestStaminaReductionsArrayObject{},
		CharacterQuests:           []CharacterQuestsArrayObject{},
		PlayerOfferQuests:         []PlayerOfferQuestsArrayObject{},
		LastPlayedChapterQuestIds: []int64{int64(res.LastPlayedChapterQuestIdPart1), int64(res.LastPlayedChapterQuestIdPart2)},
		PlayedOpenChapterIds:      []int64{int64(res.PlayedOpenChapterIdPart1), int64(res.PlayedOpenChapterIdPart2)},
		ReadGroups:                make([]interface{}, 0),
		// Template responses
		ServerVersion:       success.ServerVersion,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		NewAchievementCount: success.NewAchievementCount,
	}
}
