/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type RemoveAllPlayerFieldPartyMemberRequest struct {
	ManagedPartyMemberIds []int64 `json:"managedPartyMemberIds"`
}

// AssertRemoveAllPlayerFieldPartyMemberRequestRequired checks if the required fields are not zero-ed
func AssertRemoveAllPlayerFieldPartyMemberRequestRequired(obj RemoveAllPlayerFieldPartyMemberRequest) error {
	elements := map[string]interface{}{
		"managedPartyMemberIds": obj.ManagedPartyMemberIds,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseRemoveAllPlayerFieldPartyMemberRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of RemoveAllPlayerFieldPartyMemberRequest (e.g. [][]RemoveAllPlayerFieldPartyMemberRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseRemoveAllPlayerFieldPartyMemberRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aRemoveAllPlayerFieldPartyMemberRequest, ok := obj.(RemoveAllPlayerFieldPartyMemberRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertRemoveAllPlayerFieldPartyMemberRequestRequired(aRemoveAllPlayerFieldPartyMemberRequest)
	})
}
