package interfaces

import (
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllRoomObjectResponse(roomObjects []*model_room_object.RoomObject, success *response.BaseResponse) *GetAllRoomObjectResponse {
	roomObjectsArrayObject := make([]RoomObjectsArrayObject, len(roomObjects))
	for i, roomObject := range roomObjects {
		var outRoomObject RoomObjectsArrayObject
		calc.Copy(&outRoomObject, &roomObject)
		outRoomObject.BargainEndAt = response.ToSparkleTime(roomObject.BargainEndAt)
		outRoomObject.BargainStartAt = response.ToSparkleTime(roomObject.BargainStartAt)
		outRoomObject.DispEndAt = response.ToSparkleTime(roomObject.DispEndAt)
		outRoomObject.DispStartAt = response.ToSparkleTime(roomObject.DispStartAt)
		roomObjectsArrayObject[i] = outRoomObject
	}
	return &GetAllRoomObjectResponse{
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
		RoomObjects:         roomObjectsArrayObject,
	}
}
