/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type OrderPlayerQuestLogRequest struct {

	// Internal battle party id
	ManagedBattlePartyId int64 `json:"managedBattlePartyId"`

	// Quest state zlibed json object or null if scenario only
	QuestData *string `json:"questData"`

	// Quest id
	QuestId int64 `json:"questId"`

	// NPC characterId from npcs
	QuestNpcId int64 `json:"questNpcId"`

	// Staminas to consume
	QuestStamina int64 `json:"questStamina"`

	// Support characterId from friends
	SupportCharacterId int64 `json:"supportCharacterId"`
}

// AssertOrderPlayerQuestLogRequestRequired checks if the required fields are not zero-ed
func AssertOrderPlayerQuestLogRequestRequired(obj OrderPlayerQuestLogRequest) error {
	elements := map[string]interface{}{
		"managedBattlePartyId": obj.ManagedBattlePartyId,
		"questId":              obj.QuestId,
		"questNpcId":           obj.QuestNpcId,
		"supportCharacterId":   obj.SupportCharacterId,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseOrderPlayerQuestLogRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of OrderPlayerQuestLogRequest (e.g. [][]OrderPlayerQuestLogRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseOrderPlayerQuestLogRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aOrderPlayerQuestLogRequest, ok := obj.(OrderPlayerQuestLogRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertOrderPlayerQuestLogRequestRequired(aOrderPlayerQuestLogRequest)
	})
}
