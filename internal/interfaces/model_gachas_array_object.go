/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type GachasArrayObject struct {
	DrawPoints []interface{} `json:"drawPoints"`

	Gacha GachasArrayObjectGacha `json:"gacha"`

	Gem10CurrentStep int64 `json:"gem10CurrentStep"`

	Gem10Daily int64 `json:"gem10Daily"`

	Gem10FreeDrawCount int64 `json:"gem10FreeDrawCount"`

	Gem10Total int64 `json:"gem10Total"`

	Gem1Daily int64 `json:"gem1Daily"`

	Gem1FreeDrawCount int64 `json:"gem1FreeDrawCount"`

	Gem1Total int64 `json:"gem1Total"`

	HighRarity bool `json:"highRarity"`

	ItemDaily int64 `json:"itemDaily"`

	ItemTotal int64 `json:"itemTotal"`

	PlayerDrawPoint int64 `json:"playerDrawPoint"`

	SelectionCharacterIds []int64 `json:"selectionCharacterIds"`

	UGem1Daily int64 `json:"uGem1Daily"`

	UGem1Total int64 `json:"uGem1Total"`
}

// AssertGachasArrayObjectRequired checks if the required fields are not zero-ed
func AssertGachasArrayObjectRequired(obj GachasArrayObject) error {
	elements := map[string]interface{}{
		"drawPoints":            obj.DrawPoints,
		"gacha":                 obj.Gacha,
		"gem10CurrentStep":      obj.Gem10CurrentStep,
		"gem10Daily":            obj.Gem10Daily,
		"gem10FreeDrawCount":    obj.Gem10FreeDrawCount,
		"gem10Total":            obj.Gem10Total,
		"gem1Daily":             obj.Gem1Daily,
		"gem1FreeDrawCount":     obj.Gem1FreeDrawCount,
		"gem1Total":             obj.Gem1Total,
		"highRarity":            obj.HighRarity,
		"itemDaily":             obj.ItemDaily,
		"itemTotal":             obj.ItemTotal,
		"playerDrawPoint":       obj.PlayerDrawPoint,
		"selectionCharacterIds": obj.SelectionCharacterIds,
		"uGem1Daily":            obj.UGem1Daily,
		"uGem1Total":            obj.UGem1Total,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	if err := AssertGachasArrayObjectGachaRequired(obj.Gacha); err != nil {
		return err
	}
	return nil
}

// AssertRecurseGachasArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of GachasArrayObject (e.g. [][]GachasArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseGachasArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aGachasArrayObject, ok := obj.(GachasArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertGachasArrayObjectRequired(aGachasArrayObject)
	})
}
