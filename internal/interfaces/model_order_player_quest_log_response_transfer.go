package interfaces

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toOrderPlayerQuestLogResponse(
	u *model_user.User,
	waves *[][]*model_quest.QuestWave,
	waveDrops *[][][]model_quest.QuestWaveDrop,
	success *response.BaseResponse,
) *OrderPlayerQuestLogResponse {
	// Format item summary
	outItemSummary := make([]ItemSummaryArrayObject, 0)
	calc.Copy(&outItemSummary, &u.ItemSummary)

	// Format enemy waves
	outMWaves := make([]MWavesArrayObject, 0)
	for _, wave := range *waves {
		if len(wave) == 0 {
			continue
		}
		outWave := make([]MEnemiesArrayObject, 0)
		for _, enemy := range wave {
			outWave = append(outWave, MEnemiesArrayObject{
				MDropID:  enemy.EnemyDropId,
				MEnemyID: enemy.EnemyId,
				MEnemyLv: int64(enemy.EnemyLv),
				MScale:   float32(enemy.EnemyDisplayScale),
				Point:    int64(enemy.Point),
			})
		}
		outMWaves = append(outMWaves, MWavesArrayObject{
			MEnemies: outWave,
		})
	}

	// Format dropItems
	outDropItem := make([]DropItemArrayObject, 0)
	for _, wave := range *waveDrops {
		if len(wave) == 0 {
			continue
		}
		outWaveDrop := make([]ItemsArrayObject, 0)
		for _, enemy := range wave {
			outEnemyDrop := make([]ItemsDatasArrayObject, 0)
			for _, drop := range enemy {
				outEnemyDrop = append(outEnemyDrop, ItemsDatasArrayObject{
					Id:     int64(drop.DropItemId),
					Num:    int64(drop.DropItemAmount),
					ExtNum: 0,
				})
			}
			outWaveDrop = append(outWaveDrop, ItemsArrayObject{
				Datas: outEnemyDrop,
			})
		}
		outDropItem = append(outDropItem, DropItemArrayObject{
			Items: outWaveDrop,
		})
	}

	// Format player
	var outPlayer BasePlayer
	calc.Copy(&outPlayer, &u)
	outPlayer.CreatedAt = response.ToSparkleTime(u.CreatedAt)
	outPlayer.LastLoginAt = response.ToSparkleTime(u.LastLoginAt)
	outPlayer.LastPartyAdded = "0001-01-01T00:00:00"
	outPlayer.StaminaUpdatedAt = response.ToSparkleTime(u.StaminaUpdatedAt)

	return &OrderPlayerQuestLogResponse{
		DropItem:            outDropItem,
		ItemSummary:         outItemSummary,
		NewAchievementCount: success.NewAchievementCount,
		OccurEnemy: OrderPlayerQuestLogResponseOccurEnemy{
			MWaves: outMWaves,
		},
		OrderReceiveId: int64(u.LatestQuestLogID),
		Player:         outPlayer,
		ResultCode:     success.ResultCode,
		ResultMessage:  success.ResultMessage,
		ServerTime:     success.ServerTime,
		ServerVersion:  success.ServerVersion,
	}
}
