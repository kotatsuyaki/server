package repository

import (
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
)

type PresentRepository interface {
	FindPresent(query *model_present.Present, criteria map[string]interface{}, associations *[]string) (*model_present.Present, error)
	FindPresents(query *model_present.Present, criteria map[string]interface{}, associations *[]string) ([]*model_present.Present, error)
}
