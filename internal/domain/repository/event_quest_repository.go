package repository

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
)

type EventQuestRepository interface {
	GetEventQuest(internalUserId uint, eventQuestId uint) (*model_quest.EventQuest, error)
	GetEventQuests(internalUserId uint) ([]*model_quest.EventQuest, error)
	GetEventQuestPeriods(internalUserId uint) ([]*model_quest.EventQuestPeriod, error)
}
