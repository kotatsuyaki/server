package repository

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
)

type WeaponRecipeRepository interface {
	FindByRecipeId(recipeId uint32) (*model_weapon.WeaponRecipe, error)
}
