package repository

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestRepository interface {
	FindQuest(query *model_quest.Quest, criteria map[string]interface{}, associations []string) (*model_quest.Quest, error)
	FindQuests(query *model_quest.Quest, criteria map[string]interface{}, associations []string) ([]*model_quest.Quest, error)
	GetQuestCategory(questId uint) (*value_quest.QuestCategoryType, error)
}
