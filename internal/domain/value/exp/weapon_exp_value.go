package value_exp

import "errors"

type WeaponExp uint64

func NewWeaponExp(exp uint32) (WeaponExp, error) {
	if exp == 0 {
		return WeaponExp(0), nil
	}
	// NOTE: Minimal weapon exp was 100 at item id 5
	if exp < 100 {
		return WeaponExp(0), errors.New("too low weapon exp (it must be bigger than 100)")
	}
	// NOTE: Maximum weapon exp was 1500 at item id 900
	if exp > 1500 {
		return WeaponExp(0), errors.New("too low weapon exp (it must be lower than 1500)")
	}
	return WeaponExp(exp), nil
}
