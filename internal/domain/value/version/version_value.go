package value_version

import (
	"errors"
)

type Version string

const (
	// Online (icon change?)
	Version3_2_10 Version = "3.2.10"
	// Online (icon change?)
	Version3_2_11 Version = "3.2.11"
	// Online (icon change?)
	Version3_2_12 Version = "3.2.12"
	// Online (icon change?)
	Version3_2_13 Version = "3.2.13"
	// Online (icon change?)
	Version3_2_14 Version = "3.2.14"
	// Online (icon change?)
	Version3_2_15 Version = "3.2.15"
	// Online (icon change?)
	Version3_2_16 Version = "3.2.16"
	// Online (offline support 1)
	Version3_2_17 Version = "3.2.17"
	// Online (offline support 2)
	Version3_4_0 Version = "3.4.0"
	// Online (offline support 3) (last)
	Version3_6_0 Version = "3.6.0"
	// Offline (maybe not working)
	Version3_7_0 Version = "3.7.0"
)

var ErrInvalidVersion = errors.New("invalid version")

func GetVersionList() []Version {
	return []Version{
		Version3_2_10,
		Version3_2_11,
		Version3_2_12,
		Version3_2_13,
		Version3_2_14,
		Version3_2_15,
		Version3_2_16,
		Version3_2_17,
		Version3_4_0,
		Version3_6_0,
		Version3_7_0,
	}
}

func NewVersion(v string) (Version, error) {
	versions := GetVersionList()
	for i := range versions {
		ver := Version(v)
		if versions[i] == ver {
			return ver, nil
		}
	}
	return Version3_7_0, ErrInvalidVersion
}
