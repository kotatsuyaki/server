package value_room

import "errors"

type RoomNo int8

const (
	RoomNoBackground RoomNo = iota - 1
	RoomNoMainRoom
	RoomNoBedRoom
)

var ErrInvalidRoomNo = errors.New("invalid RoomNo. RoomNo must be in -1 to 1")

func NewRoomNo(value int8) (RoomNo, error) {
	if value < int8(RoomNoBackground) || value > int8(RoomNoBedRoom) {
		return 0, ErrInvalidRoomNo
	}
	return RoomNo(value), nil
}
