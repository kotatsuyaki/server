package value_room

import "errors"

type RoomObjectPosition uint8

var ErrInvalidRoomObjectPosition = errors.New("invalid RoomObjectPosition. position must be in 0 to 11")

func NewRoomObjectPosition(value uint8) (RoomObjectPosition, error) {
	// Basically it is at 0 to 8 but sometimes wall things can be 9 or 10 or 11? (need research)
	if value > 11 {
		return 0, ErrInvalidRoomObjectPosition
	}
	return RoomObjectPosition(value), nil
}
