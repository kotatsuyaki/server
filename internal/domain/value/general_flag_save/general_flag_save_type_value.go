package value_general_flag_save

import "errors"

type GeneralFlagSaveType uint8

const (
	GeneralFlagSaveTypePart1 GeneralFlagSaveType = iota
	GeneralFlagSaveTypePart2
)

var ErrInvalidGeneralFlagSaveType = errors.New("invalid general flag save type")

func NewGeneralFlagSaveType(t uint8) (GeneralFlagSaveType, error) {
	if t > 1 {
		return 0, ErrInvalidGeneralFlagSaveType
	}
	return GeneralFlagSaveType(t), nil
}
