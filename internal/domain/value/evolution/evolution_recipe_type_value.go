package value_evolution

type EvolutionRecipeType uint8

const (
	EvolutionRecipeTypeDefault = iota
	// Unused type
	EvolutionRecipeTypeTitleItem
)
