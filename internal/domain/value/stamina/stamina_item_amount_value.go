package value_stamina

import "errors"

type StaminaItemAmount uint16

const StaminaItemAmountForGem StaminaItemAmount = 10

var ErrInvalidStaminaItemAmount = errors.New("invalid stamina item amount")

func NewStaminaItemAmount(value uint16) (StaminaItemAmount, error) {
	if value < 1 || value > 999 {
		return 0, ErrInvalidStaminaItemAmount
	}
	return StaminaItemAmount(value), nil
}
