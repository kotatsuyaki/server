package value_stamina

import "errors"

type StaminaItemType int16

const (
	StaminaItemTypeBronze StaminaItemType = 1000 + iota
	StaminaItemTypeSilver
	StaminaItemTypeGold
	StaminaItemTypeNone StaminaItemType = -1
)

var ErrInvalidStaminaItemType = errors.New("invalid stamina item type")

func NewStaminaItemType(value int16) (StaminaItemType, error) {
	if value != int16(StaminaItemTypeBronze) && value != int16(StaminaItemTypeSilver) && value != int16(StaminaItemTypeGold) && value != int16(StaminaItemTypeNone) {
		return StaminaItemTypeNone, ErrInvalidStaminaItemType
	}
	return StaminaItemType(value), nil
}
