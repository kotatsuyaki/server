package value_character

import "errors"

// Alternative name: job type
type ClassType int8

const (
	ClassTypeNone ClassType = iota - 1
	ClassTypeFighter
	ClassTypeMagician
	ClassTypePriest
	ClassTypeKnight
	ClassTypeAlchemist
)

var ErrInvalidClassType = errors.New("invalid ClassType")

func NewClassType(v uint8) (ClassType, error) {
	if v > uint8(ClassTypeAlchemist) {
		return ClassTypeNone, ErrInvalidClassType
	}
	return ClassType(v), nil
}
