package model

type GetAllPlayerAchievementResponse struct {
	Achievements []AchievementsArrayObject
}
