package model

type ManagedRoomsArrayObject struct {
	Active int64

	ArrangeData []ArrangeDataArrayObject

	FloorId int64

	GroupId int64

	ManagedRoomId int64
}
