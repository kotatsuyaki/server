package model

type GetPlayerBadgeResponse struct {
	FriendProposedCount int64

	PresentCount int64

	TrainingCount int64
}
