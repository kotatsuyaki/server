package model

type SalePlayerWeaponResponse struct {
	ManagedWeapons []ManagedWeaponsArrayObject

	Player BasePlayer
}
