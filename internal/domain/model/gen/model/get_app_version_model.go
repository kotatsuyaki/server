package model

type GetAppVersionResponse struct {
	AbVer int64

	ResourceVersionHash string
}
