package model

type ManagedMasterOrbsArrayObject struct {
	Exp int64

	Level int64

	ManagedMasterOrbId int64

	MasterOrbId int64
}
