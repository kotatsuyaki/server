package model

type ScheduleTableStructMDayListUpInner struct {
	MTable           []ScheduleTableStructMDayListUpInnerMTableInner
	MSettingTime     int64
	MChangeStateTime int64
}
