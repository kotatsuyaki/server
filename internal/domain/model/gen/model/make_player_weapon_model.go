package model

type MakePlayerWeaponResponse struct {
	ItemSummary []ItemSummaryArrayObject

	ManagedWeapons []ManagedWeaponsArrayObject

	Player BasePlayer
}
