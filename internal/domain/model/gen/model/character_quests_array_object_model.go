package model

type CharacterQuestsArrayObject struct {
	CharacterId int64

	ClearRank ClearRank

	IsRead int64

	Quest CharacterQuestsArrayObjectQuest
}
