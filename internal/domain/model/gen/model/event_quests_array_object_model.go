package model

type EventQuestsArrayObject struct {
	ChestCostItemAmount int64

	ChestCostItemId int64

	ClearRank int64

	EventQuest EventQuestsArrayObjectEventquest

	IsRead int64

	OrderDaily int64

	OrderTotal int64
}
