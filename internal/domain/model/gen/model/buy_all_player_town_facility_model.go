package model

type BuyAllPlayerTownFacilityResponse struct {
	ManagedTownFacilityIds string

	Player BasePlayer
}
