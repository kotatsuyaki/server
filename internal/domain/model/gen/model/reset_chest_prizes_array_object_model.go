package model

type ResetChestPrizesArrayObject struct {
	CurrentStock int64

	MaxStock int64

	ResetTarget int64

	RewardAmount int64

	RewardId int64

	RewardType int64
}
