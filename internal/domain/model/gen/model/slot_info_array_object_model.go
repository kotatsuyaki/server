package model

type SlotInfoArrayObject struct {
	ManagedCharacterIds []int64

	NeedRank int64

	OpenState int64

	OrderAt string

	OrderId int64

	SlotId int64

	TrainingData *SlotInfoArrayObjectTrainingdata
}
