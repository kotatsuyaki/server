package model

type SignupPlayerResponse struct {
	AccessToken string

	PlayerId int64
}
