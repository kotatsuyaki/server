package model

type GetAllPlayerContentRoomMemberResponse struct {
	ManagedCharacterIds []int64
}
