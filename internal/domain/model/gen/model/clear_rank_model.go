package model

type ClearRank int32

const (
	_0 ClearRank = 0
	_1 ClearRank = 1
	_2 ClearRank = 2
	_3 ClearRank = 3
)
