package model

type RetryPlayerQuestLogResponse struct {
	OrderReceiveId int64

	Player BasePlayer
}
