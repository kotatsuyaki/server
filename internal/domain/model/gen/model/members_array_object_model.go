package model

type MembersArrayObject struct {
	LiveIdx int64

	ManagedCharacterId int64

	ManagedPartyMemberId int64

	RoomId int64
}
