package model

type ItemUpPlayerTownFacilityResponse struct {
	ItemSummary []ItemSummaryArrayObject

	Player BasePlayer
}
