package model

type TrainingInfoArrayObject struct {
	Category int64

	Clear int64

	ClearTime int64

	CondCharaLv int64

	CondPartyNum int64

	Cost int64

	CostType int64

	FirstGem int64

	LimitNum int64

	Name string

	Num int64

	OpenRank int64

	RewardCharaExp int64

	RewardCurrencyAmount int64

	RewardCurrencyType int64

	RewardFriendship int64

	Rewards []RewardsArrayObject

	SkipGem int64

	TrainingId int64
}
