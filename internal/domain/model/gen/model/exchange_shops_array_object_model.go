package model

type ExchangeShopsArrayObject struct {
	BadgeType int64

	BuyCount int64

	Description string

	DetailText string

	EndAt *string

	Id int64

	RestockAt *string

	RestockType int64

	RewardAmount int64

	RewardId int64

	RewardType int64

	SrcAmount int64

	SrcId int64

	SrcType int64

	StartAt *string

	Stock int64

	UiPriority int64
}
