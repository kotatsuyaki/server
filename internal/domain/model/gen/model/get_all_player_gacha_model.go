package model

type GetAllPlayerGachaResponse struct {
	FinishedGachas []interface{}

	GachaDailyFrees []interface{}

	GachaFreeNextRefreshAt string

	// Gachas []GachasArrayObject
}
