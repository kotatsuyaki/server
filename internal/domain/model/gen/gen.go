//go:generate go run .
//go:generate gofmt -w .

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	commentRegexp             = regexp.MustCompile(`(?m)^\s*\/\/.*\n?|\/\*[\s\S]*?\*\/`)
	newAchievementCountRegexp = regexp.MustCompile(`NewAchievementCount\s+\w+\s+` + "`" + `json:"\w+"` + "`")
	resultCodeRegexp          = regexp.MustCompile(`ResultCode\s+\w+\s+` + "`" + `json:"\w+"` + "`")
	resultMessageRegexp       = regexp.MustCompile(`ResultMessage\s+\w+\s+` + "`" + `json:"\w+"` + "`")
	serverTimeRegexp          = regexp.MustCompile(`ServerTime\s+\w+\s+` + "`" + `json:"\w+"` + "`")
	serverVersionRegexp       = regexp.MustCompile(`ServerVersion\s+\w+\s+` + "`" + `json:"\w+"` + "`")
	assetFuncRegexp           = regexp.MustCompile(`func\sAssert.*?\{(.|\s)*?\n}`)
	emptyStructRegexp         = regexp.MustCompile(`type\s+\w+\s+struct\s*\{\s*\}`)
	jsonTagRegexp             = regexp.MustCompile("`" + `json:"\w+"` + "`")
)

func main() {
	inputDir := "../../../interfaces"
	outputDir := "./model"

	if err := filepath.Walk(inputDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() || !strings.Contains(path, "model_") || strings.Contains(path, "_request") {
			return nil
		}

		code, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		formattedCode := formatCode(string(code))

		filename := filepath.Base(path)
		if err := writeFormattedCodeToFile(outputDir, filename, formattedCode); err != nil {
			return err
		}

		return nil
	}); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}

func formatCode(code string) []byte {
	code = commentRegexp.ReplaceAllString(code, "")
	code = newAchievementCountRegexp.ReplaceAllString(code, "")
	code = resultMessageRegexp.ReplaceAllString(code, "")
	code = resultCodeRegexp.ReplaceAllString(code, "")
	code = serverTimeRegexp.ReplaceAllString(code, "")
	code = serverVersionRegexp.ReplaceAllString(code, "")
	code = assetFuncRegexp.ReplaceAllString(code, "")
	code = emptyStructRegexp.ReplaceAllString(code, "")
	code = jsonTagRegexp.ReplaceAllString(code, "")

	// Rename package name to model
	code = strings.Replace(code, "package interfaces", "package model", 1)

	if strings.TrimSpace(code) == "package model" {
		return []byte{}
	}

	return []byte(code)
}

func writeFormattedCodeToFile(outputDir, filename string, formattedCode []byte) error {
	if len(formattedCode) == 0 {
		return nil
	}
	outputPath := filepath.Join(outputDir, filename)
	outputPath = strings.Replace(outputPath, "_response", "", 1)
	outputPath = strings.Replace(outputPath, "model_", "", 1)
	outputPath = strings.Replace(outputPath, ".go", "_model.go", 1)

	if err := os.WriteFile(outputPath, formattedCode, 0644); err != nil {
		return err
	}
	return nil
}
