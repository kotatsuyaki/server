package model_weapon

type WeaponRecipeMaterial struct {
	Id       uint32 `gorm:"primaryKey"`
	RecipeId uint32
	ItemId   uint32
	Amount   uint32
}
