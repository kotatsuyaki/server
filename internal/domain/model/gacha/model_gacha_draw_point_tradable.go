package model_gacha

type GachaDrawPointTradable struct {
	Point       uint16
	CharacterId uint64
	// Foreign key
	GachaId uint
}
