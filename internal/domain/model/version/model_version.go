package model_version

import (
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gorm.io/gorm"
)

type Version struct {
	gorm.Model
	Platform            value_version.Platform
	Version             value_version.Version
	AbVer               uint32
	ResourceVersionHash string `gorm:"column:resource_version_hash;type:varchar(32);unique_index"`
}

func NewVersion(platform value_version.Platform, version value_version.Version, abVer uint32, resourceVersionHash string) Version {
	return Version{
		Platform:            platform,
		Version:             version,
		AbVer:               abVer,
		ResourceVersionHash: resourceVersionHash,
	}
}
