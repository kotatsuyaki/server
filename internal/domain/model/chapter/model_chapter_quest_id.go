package model_chapter

type ChapterQuestId struct {
	Id uint `gorm:"primary_key"`
	// Foreign key
	ChapterId uint
	QuestId   int32
}
