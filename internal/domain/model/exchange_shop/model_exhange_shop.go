package model_exchange_shop

import (
	"time"

	value_exchange_shop "gitlab.com/kirafan/sparkle/server/internal/domain/value/exchange_shop"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_shared "gitlab.com/kirafan/sparkle/server/internal/domain/value/shared"
)

type ExchangeShop struct {
	Id          int64                           `json:"id"`
	StartAt     *time.Time                      `json:"startAt"`
	EndAt       *time.Time                      `json:"endAt"`
	RestockType value_exchange_shop.RestockType `json:"restockType"`
	// how many times the item can be bought (-1 means unlimited)
	Stock      int16                   `json:"stock"`
	UiPriority value_shared.UiPriority `json:"uiPriority"`
	// Maybe trade want object? (looks like this must be TypeUnlimitedGem)
	SrcType value_present.PresentType `json:"srcType"`
	// Sometimes (gold, gems or etc) this can be -1
	SrcId     int64  `json:"srcId"`
	SrcAmount uint32 `json:"srcAmount"`
	// Maybe trade give object? (looks like this must be TypePackageItem)
	RewardType value_present.PresentType `json:"rewardType"`
	// this field should be packageItemId otherwise client doesn't show item info
	RewardId int64 `json:"rewardId"`
	// this field should be 1
	RewardAmount uint32 `json:"rewardAmount"`
	// unused field?
	Description string `json:"description"`
	// message inside the help button (TextMeshPro supported)
	DetailText string `json:"detailText"`
	// how many times this item bought by player
	BuyCount uint16 `json:"buyCount"`
	// New / Update / None / Unknown
	BadgeType value_exchange_shop.BadgeType `json:"badgeType"`
	RestockAt *time.Time                    `json:"restockAt"`
}
