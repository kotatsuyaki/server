package model_event_banner

type EventBanner struct {
	// Database primary key
	Id int64 `json:"id"`

	StartAt   string `json:"startAt"`
	DispEndAt string `json:"dispEndAt"`
	// maybe deprecated
	EndAt string `json:"endAt"`

	// TODO
	Category int64 `json:"category"`
	// maybe same as gacha/get_all's id
	GachaId int64 `json:"gachaId"`
	// Banner image filename
	ImgId string `json:"imgId"`

	// maybe deprecated
	OffsetX float32 `json:"offsetX"`
	// maybe deprecated
	OffsetY float32 `json:"offsetY"`
	// maybe deprecated
	PickupCharacterIds *string `json:"pickupCharacterIds"`
}
