package model_quest

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type EventQuestWrapper struct {
	EventQuest EventQuest
	ClearRank  value_quest.ClearRank
	IsRead     value.BoolLikeUInt8

	OrderDaily uint32
	OrderTotal uint32

	ChestCostItemId     int32 // this value can be -1
	ChestCostItemAmount uint32
}
