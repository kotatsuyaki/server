package model_quest

type EventQuestCondId struct {
	Id               uint `gorm:"primaryKey"`
	CondEventQuestId uint
	// Foreign key
	EventQuestId uint `gorm:"foreignKey:Id"`
}
