package model_exp_table

type ExpTableCharacter struct {
	Level               uint `gorm:"primaryKey"`
	NextExp             uint32
	TotalExp            uint32
	RequiredCoinPerItem uint16
}
