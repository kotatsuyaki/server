package model_user

import (
	"time"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
)

type UserMission struct {
	ManagedMissionId uint `gorm:"primary_key"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time `sql:"index"`
	LimitTime        time.Time
	Rate             uint
	State            value_mission.MissionState
	// foreignKey
	MissionId uint
	Mission   model_mission.Mission
	// foreignKey
	UserId uint
	User   User `gorm:"PRELOAD:false"`
}
