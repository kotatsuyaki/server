package model_user

import value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"

func newManagedWeapon(userId uint, weaponId value_weapon.WeaponId) ManagedWeapon {
	return ManagedWeapon{
		WeaponId:   weaponId,
		Level:      1,
		Exp:        0,
		SkillLevel: 1,
		SkillExp:   0,
		State:      1,
		SoldAt:     nil,
	}
}
