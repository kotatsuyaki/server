package model_user

import (
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestClearRank struct {
	QuestId   uint
	ClearRank value_quest.ClearRank
}

type QuestClearRanks []*QuestClearRank

func (qcr *QuestClearRanks) GetClearRank(questId uint) value_quest.ClearRank {
	for _, qcr := range *qcr {
		if qcr.QuestId == questId {
			return qcr.ClearRank
		}
	}
	return value_quest.ClearRankNone
}
