package persistence

import (
	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type itemTableTownFacilityRepositoryImpl struct {
	Conn *gorm.DB
}

func NewItemTableTownFacilityRepositoryImpl(conn *gorm.DB) repository.ItemTableTownFacilityRepository {
	return &itemTableTownFacilityRepositoryImpl{Conn: conn}
}

func (rp *itemTableTownFacilityRepositoryImpl) FindItemTableTownFacility(itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error) {
	var datas []*model_item_table.ItemTableTownFacility
	result := rp.Conn.Where(model_item_table.ItemTableTownFacility{
		ItemNo: itemNo,
	}).Find(&datas)
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}
