package persistence

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_evolution "gitlab.com/kirafan/sparkle/server/internal/domain/value/evolution"

	"gorm.io/gorm"
)

type evoTableEvolutionRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEvoTableEvolutionRepositoryImpl(conn *gorm.DB) repository.EvoTableEvolutionRepository {
	return &evoTableEvolutionRepositoryImpl{Conn: conn}
}

func (rp *evoTableEvolutionRepositoryImpl) FindByCharacterId(characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error) {
	var data *model_evo_table.EvoTableEvolution
	if result := rp.Conn.Preload("RequiredItems").Where(&model_evo_table.EvoTableEvolution{
		SrcCharacterId: characterId,
		// TODO: Implement EvolutionRecipeTypeTitleItem and check client works or not
		RecipeType: value_evolution.EvolutionRecipeTypeDefault,
	}).Find(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
