package persistence

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type eventQuestRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEventQuestRepositoryImpl(conn *gorm.DB) repository.EventQuestRepository {
	return &eventQuestRepositoryImpl{Conn: conn}
}

func (qr *eventQuestRepositoryImpl) GetEventQuest(internalUserId uint, eventQuestId uint) (*model_quest.EventQuest, error) {
	var eventQuest *model_quest.EventQuest
	result := qr.Conn.Preload("Quest").Preload("CondEventQuestIds").Where("id = ?", eventQuestId).Find(&eventQuest)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuest, nil
}

func (qr *eventQuestRepositoryImpl) GetEventQuests(internalUserId uint) ([]*model_quest.EventQuest, error) {
	var eventQuests []*model_quest.EventQuest
	result := qr.Conn.Preload("Quest").Preload("CondEventQuestIds").Find(&eventQuests)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuests, nil
}

func (qr *eventQuestRepositoryImpl) GetEventQuestPeriods(internalUserId uint) ([]*model_quest.EventQuestPeriod, error) {
	var eventQuestPeriods []*model_quest.EventQuestPeriod
	result := qr.Conn.Preload(
		"EventQuestPeriodGroups.EventQuestPeriodGroupQuests.CondEventQuestIds",
	).Find(&eventQuestPeriods)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuestPeriods, nil
}
