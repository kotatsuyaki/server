package persistence

import (
	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type characterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewCharacterRepositoryImpl(conn *gorm.DB) repository.CharacterRepository {
	return &characterRepositoryImpl{Conn: conn}
}

func (rp *characterRepositoryImpl) FindCharacters(query *model_character.Character, criteria map[string]interface{}, associations *[]string) ([]*model_character.Character, error) {
	var datas []*model_character.Character
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *characterRepositoryImpl) FindCharacter(query *model_character.Character, criteria map[string]interface{}, associations *[]string) (*model_character.Character, error) {
	var data *model_character.Character
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
