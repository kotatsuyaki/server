package persistence

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"

	"gorm.io/gorm"
)

type weaponRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponRepositoryImpl(conn *gorm.DB) repository.WeaponRepository {
	return &weaponRepositoryImpl{Conn: conn}
}

func (rp *weaponRepositoryImpl) FindByWeaponId(weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error) {
	var data *model_weapon.Weapon
	result := rp.Conn.Preload("UpgradeBonusMaterialItemIDs").Where("weapon_id = ?", weaponId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
