package middleware

import (
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
)

type middleware func(next http.HandlerFunc) http.HandlerFunc

type Middlewares []middleware

func GetAuthIgnoredRoutes() []string {
	return []string{
		"/api/app/version/get",
		"/api/player/import/offline",
		"/api/player/move/set",
		"/api/player/signup",
		"/api/player/login",
		"/api/player/reset",
	}
}

func GetMiddlewares(key encrypt.Key, padding encrypt.Pad, ss repository.SessionRepository, lr repository.LoggerRepository) Middlewares {
	ignoreRoutes := GetAuthIgnoredRoutes()
	var ms []middleware
	if key != "" && padding != "" {
		ms = []middleware{
			NewAppDecryptionMiddleware(&ignoreRoutes, &key, &padding),
			NewAppSessionMiddleware(ignoreRoutes, ss),
			NewLoggerMiddleware(lr),
			NewAppEncryptionMiddleware(&ignoreRoutes, &key, &padding),
			NewCorsMiddleware(),
		}
	} else {
		ms = []middleware{
			NewAppSessionMiddleware(ignoreRoutes, ss),
			NewLoggerMiddleware(lr),
			NewCorsMiddleware(),
		}
	}
	return ms
}

func (m Middlewares) Then(h http.HandlerFunc) http.HandlerFunc {
	for i := range m {
		h = m[len(m)-1-i](h)
	}
	return h
}
