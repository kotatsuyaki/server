package database

import (
	"os"

	"github.com/joho/godotenv"
)

type DatabaseType = int32

const (
	DatabaseTypeSqliteInMemory DatabaseType = iota
	DatabaseTypeSqlite
	DatabaseTypeMysql
)

// ConfigList stores credentials
type ConfigList struct {
	DatabaseType    DatabaseType
	DatabaseName    string
	DatabaseHost    string
	DatabasePort    string
	DatabaseUser    string
	DatabasePass    string
	DatabaseVerbose bool
}

// GetConfig creates ConfigList from environment variables
func GetConfig() ConfigList {
	if envFilePath := os.Getenv("GO_ENV"); envFilePath != "" {
		if err := godotenv.Load(envFilePath); err != nil {
			panic("envFile " + envFilePath + "could not be loaded")
		}
	} else {
		_ = godotenv.Load(".env")
	}
	var dbType DatabaseType
	switch os.Getenv("DB_TYPE") {
	case "in-memory":
		dbType = DatabaseTypeSqliteInMemory
	case "sqlite":
		dbType = DatabaseTypeSqlite
	case "mysql":
		dbType = DatabaseTypeMysql
	}
	// Parse to ConfigList struct
	return ConfigList{
		DatabaseType:    dbType,
		DatabaseName:    os.Getenv("DB_NAME"),
		DatabaseHost:    os.Getenv("DB_HOST"),
		DatabasePort:    os.Getenv("DB_PORT"),
		DatabaseUser:    os.Getenv("DB_USER"),
		DatabasePass:    os.Getenv("DB_PASS"),
		DatabaseVerbose: os.Getenv("DB_VERBOSE") == "1",
	}
}
