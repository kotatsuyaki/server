package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestWaveRandoms(db *gorm.DB) {
	questWaveRandomFile, err := Read("wave_random_list")
	if err != nil {
		return
	}
	var questWaveRandoms []model_quest.QuestWaveRandom
	err = json.Unmarshal(questWaveRandomFile, &questWaveRandoms)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questWaveRandoms, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
