## Coding rules

This document describes the coding rules for the project.
This is for prevent coding confuses please follow the rules.

### 1: Response models must be int64

All response models must be int64. This is because of the limitation of the OpenAPI document.
Gorm database models should use just fit type. But if you don't know what value will be stored at the field, you can use int64.

### 2: Database time must be time.Time (not String)

Response models were in string but database models must be time.Time.

### 3: Not to use gorm.Model

It is useful but it has some problems like below. So we don't use it.

```go
type User struct {
	gorm.Model
	Name string
}

// This one doesn't work
user := User{Name: "foo", CreatedAt: time.Now(), UpdatedAt: time.Now()}

// This one works
user := User{
	Name: "foo",
    gorm.Model{
        CreatedAt: time.Now(),
		UpdatedAt: time.Now()
    }
```

### 4: Use same words as written in Sparkle API doc.

If official API saids managedTownId, we should use managedTownId not townId.
(Actually official API word has spelling variants like userID vs PlayerID, you can choose in the case.
Below are examples which word we need to use.

- Bad: Coin / Good: Gold
- Bad: Starlight Stone(星彩石) / Good: Gem

### 5: Persistence layer must be keep in simple

At early development, user persistence layer had really detailed methods.
(Ex. FindByUUID, FindByMyCode, FindBySession... etc.)
But it makes useless code at usecase layer. Also persistence layer bigger and bigger at implement.
Persistence layer must have only simple methods like below. You can refer the existing code how to do this.

- Find
- Finds
- Create
- Update
- Delete

### 6: Database model's id field names must be Id (Not ID)

It sounds weird but response models ID fields are generated as Id.
(For example: we need to use managedTownId, not managedTownID)
Since copier can't copy automatically if the names are difference like managedTownId and managedTownID,
We must keep the name as Id.