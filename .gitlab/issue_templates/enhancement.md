## Summary / 概要

<!-- Describe your proposed solution or enhancement in as much detail as possible. What new functionality would you like to see added? How would it work? What benefits would it provide? -->

## Details / 詳細

<!-- Describe the details of the proposed solution or enhancement. -->

## Additional Information / 関連資料

<!-- Include any additional information that you think might be helpful in evaluating your proposed enhancement, such as relevant research, data, or examples. -->
