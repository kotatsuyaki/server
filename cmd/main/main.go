/*
 * SparkleAPI
 * "Our tomorrow is always a prologue"
 *
 * Contact: contact@sparklefantasia.com
 */

package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/urfave/cli"
	"gitlab.com/kirafan/sparkle/server/cmd/main/bootstrap"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability/tracer_provider"
)

func main() {
	// Create cli app
	app := cli.NewApp()
	app.Name = "sparkle-api"
	app.Usage = "This executable starts sparkle-api server at port 8080"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "migrate",
			Usage: "run migration and seed (default false)",
		},
	}

	// Run cli app
	app.Action = func(c *cli.Context) error {
		// Initialize with uses bootstraps
		logger, loggerRepo, db := bootstrap.GetLoggerAndDatabase()
		router := bootstrap.CreateRouter(loggerRepo, db)
		wrappedRouter := observability.InjectObservabilityToRouter(router)

		// FIXME: This is a workaround for now
		// Please make a config object and pass it to this function
		if envFilePath := os.Getenv("GO_ENV"); envFilePath != "" {
			if err := godotenv.Load(envFilePath); err != nil {
				fmt.Print("envFile " + envFilePath + "could not be loaded")
			}
		} else {
			_ = godotenv.Load(".env")
		}
		// dsn := os.Getenv("UPTRACE_DSN")
		dsn := os.Getenv("JAEGER_DSN")
		env := os.Getenv("ENVIRONMENT")
		deployEnv := "local"
		if env != "" {
			deployEnv = env
		}

		if dsn != "" {
			// Initialize openTelemetry observability
			otelResource := observability.GetOtelResource(context.Background(), deployEnv)
			// meter_provider.InitUpTraceMetrics(context.Background(), dsn, otelResource)
			shutdown := tracer_provider.InitJaegerTracer(context.Background(), dsn, otelResource, true)
			defer shutdown(context.Background())
		}

		// Startup
		if c.Bool("migrate") {
			logger.Info("migrating database...")
			bootstrap.RunMigration(db)
			logger.Info("migrated!")
		}
		logger.Info("Server is running on port 8080")
		logger.Fatal(http.ListenAndServe("0.0.0.0:8080", wrappedRouter))
		return nil
	}
	app.Run(os.Args)
}
