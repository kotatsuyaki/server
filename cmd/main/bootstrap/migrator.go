package bootstrap

import (
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gorm.io/gorm"
)

func RunMigration(db *gorm.DB) {
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)
}
