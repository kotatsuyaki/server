package bootstrap

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gorm.io/gorm"
)

func GetLoggerAndDatabase() (*logrus.Logger, repository.LoggerRepository, *gorm.DB) {
	logger := database.InitLogger()
	loggerRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(logger)
	return logger, loggerRepo, db
}
