package bootstrap

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/wires"
	"gorm.io/gorm"
)

func CreateRouter(loggerRepo repository.LoggerRepository, db *gorm.DB) *mux.Router {
	// Create gorm and logger repo instance
	router := wires.InitializeRouter(loggerRepo, db)

	// Serve Index
	res := []byte("Sparkle API Server is working")
	router.Path("/").Handler(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
			w.Write(res)
		}),
	)
	return router
}
