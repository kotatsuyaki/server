package response

import "strings"

type BaseResponse struct {
	NewAchievementCount int32  `json:"newAchievementCount"`
	ResultCode          int32  `json:"resultCode"`
	ResultMessage       string `json:"resultMessage"`
	ServerTime          string `json:"serverTime"`
	ServerVersion       int32  `json:"serverVersion"`
	// Below values used if RESULT_INVALID_AUTH_VOIDED and login request
	Subject *string `json:"subject,omitempty"`
	Message *string `json:"message,omitempty"`
}

func NewSuccessResponse() BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_SUCCESS),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
	}
}

func NewErrorResponse(code ResultCode) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(code),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
	}
}

var notImplementedMessages = []string{
	"<size=30>未実装の機能です Unimplemented feature 这是一个未实现的功能</size>",
	"",
	"この機能は現在実装されていません、実装されることを祈ってください。",
	"This feature isn't implemented yet. Please wait for it to be implemented.",
	"该功能尚未实现。等待它实现。",
	"",
	"ご興味があれば、開発にご協力ください。",
	"If you are interested, please contribute to the server project.",
	"如果您有兴趣，请为开发做出贡献。",
	"",
	"https://gitlab.com/kirafan/sparkle/server",
}
var notImplementedMessage = strings.Join(notImplementedMessages, "\n")

func NewNotImplementedResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(notImplementedMessage)
}

func NewErrorResponseWithMessage(subject string, message string) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_INVALID_AUTH_VOIDED),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
		Subject:             &subject,
		Message:             &message,
	}
}

func NewMaintenanceResponseWithMessage(message string) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_MAINTENANCE),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
		Message:             &message,
	}
}
