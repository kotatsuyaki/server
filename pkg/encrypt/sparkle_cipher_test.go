package encrypt

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestSparkleCipher_getIV(t *testing.T) {
	key := LoadKey()
	padding := LoadPad()

	type args struct {
		path string
	}
	tests := []struct {
		name string
		k    *SparkleCipher
		args args
		want []byte
	}{
		{
			name: "getIV success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				path: "/api/player/training/get_list",
			},
			want: []byte{47, 97, 112, 105, 47, 112, 108, 97, 121, 101, 114, 47, 116, 114, 97, 105},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.k.getIV(tt.args.path); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SparkleCipher.getIV() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSparkleCipher_GetStarCC(t *testing.T) {
	key := LoadKey()
	padding := LoadPad()
	type args struct {
		body       map[string]interface{}
		serverTime string
	}
	tests := []struct {
		name string
		k    *SparkleCipher
		args args
		want string
	}{
		{
			name: "getStarCC with simple success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				body:       map[string]interface{}{"message": "ok"},
				serverTime: "2023-01-22T20:34:47",
			},
			want: "a2832967",
		},
		{
			name: "getStarCC with structured success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				body:       map[string]interface{}{"hello": "world!", "message": "ok"},
				serverTime: "2023-01-22T20:34:47",
			},
			want: "516c81c7",
		},
		{
			// NOTE: Golang json.Marshal doesn't get ordered but Python json.dumps does. Be careful!
			name: "getStarCC with array success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				body:       map[string]interface{}{"arr": []int{1, 2, 3}, "hello!": "世界!", "message": "ok"},
				serverTime: "2023-01-22T20:34:47",
			},
			want: "ea3432c9",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.k.GetStarCC(tt.args.body, tt.args.serverTime); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SparkleCipher.getIV() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSparkleCipher_DecryptAsRequest(t *testing.T) {
	key := LoadKey()
	padding := LoadPad()
	type args struct {
		path string
		body string
	}
	tests := []struct {
		name string
		k    *SparkleCipher
		args args
		want map[string]interface{}
	}{
		// TODO: Add test cases.
		{
			name: "decrypt push_token as request success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				path: "/api/player/push_token/set",
				body: "xyllN14JTu7ZWk0G3KkpaGtnH19mJgtCWg+YyKnCq8o6DaoDX5CV+I/8z4s9PVcvFS3j8XZUIXT8+M6rzTdPA169RXpYxscDcihiAzvExs9L48s5bZ+HKlQx/Sh9EgGq",
			},
			want: map[string]interface{}{
				"pushToken": "9496F60CBBC0329ED001053CA16E57DA207B3810B2206F974B652FB663D0AE10",
			},
		},
		{
			name: "decrypt as request success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				path: "/api/player/training/get_list",
				body: "PAmDt13QbC2lEa3jm9qQAwKIEPuvEs9o4z1CfDjwW2zOqzPvjANjVmDnCcTg5on7",
			},
			want: map[string]interface{}{
				"message": "ok",
				"hello": []int{
					1, 2, 3, 4, 5,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var result interface{}
			if err := tt.k.DecryptAsRequest(tt.args.path, tt.args.body, &result); err != nil {
				t.Errorf("SparkleCipher.DecryptAsRequest() = %v, want %v", err, tt.want)
				return
			}
			actual, _ := json.Marshal(result)
			expect, _ := json.Marshal(tt.want)
			if string(actual) != string(expect) {
				t.Errorf("SparkleCipher.DecryptAsRequest() = %v, want %v", result, tt.want)
			}
		})
	}
}

func TestSparkleCipher_EncryptAsResponse(t *testing.T) {
	key := LoadKey()
	padding := LoadPad()
	type args struct {
		path string
		body interface{}
	}
	tests := []struct {
		name string
		k    *SparkleCipher
		args args
		want []byte
	}{
		{
			name: "encrypt as response success",
			k:    NewSparkleCipher(key, padding),
			args: args{
				path: "/api/player/get_all",
				body: map[string]interface{}{
					"hello": []int{
						1, 2, 3, 4, 5,
					},
					"message": "ok",
				},
			},
			want: []byte{
				41, 118, 202, 204, 245,
				169, 234, 81, 223, 243, 200,
				161, 212, 247, 158, 96, 118,
				208, 132, 102, 114, 124, 40,
				63, 82, 164, 126, 115, 51,
				153, 167, 17, 240, 255,
				12, 130, 22, 132, 237,
				25, 9, 212, 17, 101, 7,
				85, 88, 181, 175, 153,
				102, 91, 0, 217, 62,
				163, 80, 220, 100, 83,
				109, 254, 161, 126,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.k.EncryptAsResponse(tt.args.path, tt.args.body); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SparkleCipher.EncryptAsResponse() = %v, want %v", got, tt.want)
			}
		})
	}
}
